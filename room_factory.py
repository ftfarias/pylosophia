from room import Room

def create_test_room():
    r = Room()
    r.description = """The test room. A simple room, barely decorated, white walls and smelling vanilla.
    Someone wrote in red letters 'MFD is the key' middle of in the east wall."""
    return r